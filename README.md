# Lost in Translation

Translate any english words to sign language!

## Description

An application for translating english to sign language. Possibilities include:

- Logging in. If your user does not already exist, it will be created automatically.
- Translating any text.
- Viewing your ten last translations.
- Clearing your translations.
- Logging out.

[Click here to check out a live demo.](https://lost-in-translation-simon.herokuapp.com/)

#

## Install

First of, clone the repository. Next, run:

```
npm install
```

(This project uses node and npm. Go check them out if you don't have them locally installed.)

## How to run

```
npm start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
