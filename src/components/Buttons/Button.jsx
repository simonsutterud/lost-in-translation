import styled from "styled-components";

const ButtonEl = styled.button`
	width: calc(50% - 1rem);
	padding: 1.5rem;
	outline: 1px solid;
	outline-color: ${(props) => (props.primary ? "var(--color-brand--2)" : "var(--color-brand--1)")};
	background-color: ${(props) =>
		props.primary ? "var(--color-brand--2)" : "var(--color-brand--1)"};
	color: var(--color-dark--1);

	border: none;
	border-radius: 10px;
	cursor: pointer;
	transition: all 300ms;
	font-weight: 800;
	font-size: 1rem;

	:hover {
		background: transparent;
		color: ${(props) => (props.primary ? "var(--color-brand--2)" : "var(--color-brand--1)")};
	}
`;

export default function Button({ children, primary, onClick }) {
	return (
		<ButtonEl onClick={onClick} primary={primary}>
			{children}
		</ButtonEl>
	);
}
