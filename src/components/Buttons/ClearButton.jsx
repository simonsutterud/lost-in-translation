import styled from "styled-components";

const ClearButtonEl = styled.button`
	padding: 1rem 2rem 2rem 2rem;

	background-color: var(--color-dark--2);
	color: var(--color-light--2);

	border: none;
	border-radius: 10px 10px 0 0;
	cursor: pointer;
	transition: all 300ms;
	font-weight: 500;
	font-size: 1rem;
	z-index: 1;

	:hover {
		background-color: #c53030;
	}
`;

export default function ClearButton({ children, onClick }) {
	return (
		<ClearButtonEl onClick={onClick} primary>
			{children}
		</ClearButtonEl>
	);
}
