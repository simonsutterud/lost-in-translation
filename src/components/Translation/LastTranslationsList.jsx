import styled from "styled-components";

const Ul = styled.ul`
	width: 100%;
	height: 100%;
	list-style: none;
	z-index: 1;
	background-color: inherit;
`;

export default function LastTranslationList({ children }) {
	return <Ul>{children}</Ul>;
}
