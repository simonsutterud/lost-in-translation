import styled from "styled-components";

const Image = styled.img`
	width: 78px;
	height: 78px;
	margin-bottom: 15px;
`;

const Space = styled.div`
	width: 78px;
	height: 78px;
`;

export default function Translation({ letter }) {
	if (letter === " ") return <Space />;
	else
		return (
			<Image src={"/individual_signs/" + letter + ".png"} alt={`${letter} in sign language`} />
		);
}
