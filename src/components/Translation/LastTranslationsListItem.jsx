import styled from "styled-components";

const Li = styled.li`
	height: 10%;
	display: flex;
	align-items: center;
	padding-left: 1rem;
	background-color: var(--color-light--3);
	overflow: auto;

	:nth-of-type(odd) {
		background-color: var(--color-light--2);
	}

	:first-of-type {
		border-radius: 10px 10px 0 0;
	}

	:nth-of-type(10) {
		border-radius: 0 0 10px 10px;
	}
`;

export default function LastTranslationListItem({ children }) {
	return <Li>{children}</Li>;
}
