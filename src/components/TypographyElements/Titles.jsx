import styled from "styled-components";

export const Title = styled.h1`
	font-size: 3rem;
	color: var(--color-dark--1);
`;

export const Subtitle = styled.h2`
	font-size: 2rem;
	color: var(--color-dark--1);
	font-weight: 300;
`;

export const PageTitle = styled.h2`
	font-size: 1.5rem;
	color: var(--color-light--2);
	font-weight: 600;
	align-self: flex-start;
	margin-top: 2rem;
`;
