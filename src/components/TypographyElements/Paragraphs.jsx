import styled from "styled-components";

export const Error = styled.p`
	position: absolute;
	margin-top: -2rem;
	color: #ff4a4a;
`;

export const Info = styled.p`
	font-size: x-large;
	color: var(--color-dark--2);
`;
