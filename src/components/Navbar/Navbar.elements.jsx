import styled from "styled-components";

export const Nav = styled.nav`
	width: 100vw;
	height: 2rem;
	padding: 2rem 0;
	box-sizing: border-box;
	background-color: var(--color-dark--1);
	display: flex;
	justify-content: center;
	align-items: center;
	border-bottom: 1px solid var(--color-dark--2);
`;

export const NavWrapper = styled.div`
	width: min(1000px, 100vw);
	padding: 0 1rem;
	box-sizing: border-box;
	display: flex;
	justify-content: space-between;
	align-items: center;
`;

export const Title = styled.h2`
	color: var(--color-light--2);
`;

export const Span = styled.span`
	color: var(--color-brand--1);
`;

export const ProfileBtn = styled.button`
	cursor: pointer;
	background-color: var(--color-dark--1);
	border: none;
	border-radius: 10px;
	color: var(--color-light--2);
	display: flex;
	justify-content: center;
	align-items: center;
	transition: all 200ms;

	:hover {
		filter: opacity(0.8);
	}
`;

export const ProfileName = styled.h5`
	font-weight: 500;
	font-size: medium;
`;

export const ProfileIcon = styled.img`
	height: 40px;
	margin-left: 0.4rem;
`;
