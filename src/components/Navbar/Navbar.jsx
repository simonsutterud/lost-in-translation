import { useSelector } from "react-redux/es/exports";
import { Link } from "react-router-dom";

import accountCircle from "../../assets/Account circle.svg";

import {
	Nav,
	NavWrapper,
	Title,
	Span,
	ProfileBtn,
	ProfileName,
	ProfileIcon,
} from "./Navbar.elements";

export default function Navbar() {
	const user = useSelector((state) => state.user);

	return (
		<Nav>
			<NavWrapper>
				<Link to="/" style={{ textDecoration: "none" }}>
					<Title>
						Lost in <Span>Translation</Span>
					</Title>
				</Link>
				{user.id && (
					<Link to="/profile" style={{ textDecoration: "none" }}>
						<ProfileBtn>
							<ProfileName>{user.username}</ProfileName>
							<ProfileIcon src={accountCircle}></ProfileIcon>
						</ProfileBtn>
					</Link>
				)}
			</NavWrapper>
		</Nav>
	);
}
