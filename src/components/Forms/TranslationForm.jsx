import { useForm } from "react-hook-form";
import { useSelector } from "react-redux/es/exports";

import { Form, Input, Button, Icon } from "./Form.elements";
import { Error } from "../TypographyElements/Paragraphs";

import arrow from "../../assets/Arrow 1.svg";

export default function TranslationForm({ handleTranslation }) {
	const username = useSelector((state) => state.user.username);

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm();

	return (
		<>
			<Form onSubmit={handleSubmit(handleTranslation)}>
				<Input
					inputWidth="100%"
					type="text"
					placeholder={`What do you want to translate, ${username}?`}
					{...register("translationText", {
						required: { value: true, message: "Please enter what you want to translate." },
						pattern: {
							value: /[A-Za-z]+/,
							message: "Invalid input! Only letters in the english alphabet are accepted.",
						},
					})}
				/>
				<Button type="submit">
					<Icon src={arrow}></Icon>
				</Button>
				{errors.translationText && <Error> {errors.translationText.message} </Error>}
			</Form>
		</>
	);
}
