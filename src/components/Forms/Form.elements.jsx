import styled from "styled-components";

export const Form = styled.form`
	width: 100%;
	display: flex;
	justify-content: center;
`;

export const Input = styled.input`
	width: ${(props) => props.inputWidth};
	padding: 2rem;
	padding-right: calc(93.5px + 2rem);
	border: none;
	background-color: var(--color-light--3);
	border-radius: 10px;
	font-size: 1.5rem;
	line-height: 1.5rem;

	::placeholder {
		color: var(--color-dark--2);
		font-weight: 500;
	}

	:focus {
		outline: 2px solid var(--color-brand--2);
	}
`;

export const Button = styled.button`
	margin-left: -93.5px;
	padding: 1rem 1.5rem;
	background-color: var(--color-dark--2);
	color: var(--color-brand--2);
	border: none;
	border-radius: 0 10px 10px 0;
	cursor: pointer;
	transition: filter 200ms;

	:hover {
		filter: brightness(0.8);
	}
`;

export const Icon = styled.img`
	height: 30px;
`;
