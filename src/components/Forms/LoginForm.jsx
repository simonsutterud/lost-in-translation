import { useForm } from "react-hook-form";

import { Form, Input, Button, Icon } from "./Form.elements";
import { Error } from "../TypographyElements/Paragraphs";
import arrow from "../../assets/Arrow 1.svg";
import { createUser, fetchUserByUsername } from "../../api/user";
import { useDispatch } from "react-redux/es/exports";
import { loggedIn } from "../../features/user/userSlice";
import { useNavigate } from "react-router-dom";
import { setLocalStorage } from "../../utils/localStorage";

export default function LoginForm({ loading, setLoading }) {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm();

	const onSubmit = async ({ username }) => {
		if (loading) return;
		setLoading(true);

		let user = await fetchUserByUsername(username);

		if (user.error) {
			// If no user was found, create new user
			({ user } = await createUser(username));
			dispatch(loggedIn(user));
			setLocalStorage("user", user);
			navigate("/");
		} else {
			// If user was found, log user in
			dispatch(loggedIn(user.user[0]));
			setLocalStorage("user", user.user[0]);
			navigate("/");
		}
		setLoading(false);
	};

	return (
		<>
			<Form onSubmit={handleSubmit(onSubmit)}>
				<Input
					inputWidth="80%"
					type="text"
					placeholder="What's your name?"
					{...register("username", { required: true, minLength: 4 })}
				/>
				<Button type="submit">
					<Icon src={arrow}></Icon>
				</Button>
				{errors.username && <Error> Please enter a valid username (min. 4 characters).</Error>}
			</Form>
		</>
	);
}
