import { useState } from "react";
import { useDispatch, useSelector } from "react-redux/es/exports";

import Section from "../hoc/Section";
import Container from "../hoc/Container";
import TranslationForm from "../components/Forms/TranslationForm";
import Translation from "../components/Translation/Translation";

import { updatedTranslations } from "../features/user/userSlice";
import { saveTranslation } from "../api/translations";
import { setLocalStorage } from "../utils/localStorage";

export default function TranslationPage() {
	const [translationLetters, setTranslationLetters] = useState([]);
	const dispatch = useDispatch();

	const userId = useSelector((state) => state.user.id);

	const handleTranslation = async ({ translationText }) => {
		const lettersArr = translationText
			.replaceAll(/[^a-zA-Z ]/g, "")
			.toLowerCase()
			.split("");
		setTranslationLetters(lettersArr);

		const { user: updatedUser } = await saveTranslation(userId, translationText);
		dispatch(updatedTranslations(updatedUser.translations));
		setLocalStorage("user", updatedUser);
	};

	return (
		<>
			<Section bgColor="var(--color-dark--1)">
				<Container height="120px" justifyContent="center" alignItems="flex-end">
					<TranslationForm width="100%" handleTranslation={handleTranslation} />
				</Container>
			</Section>
			<Section
				bgColor="var(--color-dark--1)"
				height="100vh"
				justifyContent="center"
				alignItems="center"
			>
				<Container
					padding="2rem 1rem"
					height="400px"
					width="min(calc(1000px - 2rem), (100vw - 1rem))"
					justifyContent="flex-start"
					alignItems="flex-start"
					bgColor="var(--color-light--3)"
					borderRadius="10px"
					overflow="auto"
					flexWrap="wrap"
				>
					{translationLetters &&
						translationLetters.map((letter, i) => <Translation letter={letter} key={i} />)}

					<Container
						position="absolute"
						justifySelf="flex-end"
						padding="1rem 2rem"
						bgColor="var(--color-brand--2)"
						width="min(calc(1000px - 2rem), (100vw - 1rem))"
						borderRadius="0 0px 10px 10px"
						color="var(--color-dark--1)"
						fontWeight={600}
						margin="400px 0  0"
						transform="translate(-1rem, calc(-100% - 2rem))"
					>
						Translation
					</Container>
				</Container>
			</Section>
		</>
	);
}
