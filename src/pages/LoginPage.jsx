import React, { useState } from "react";

import Container from "../hoc/Container";
import Section from "../hoc/Section";
import LoginForm from "../components/Forms/LoginForm";
import figure from "../assets/figure.svg";

import { Subtitle, Title } from "../components/TypographyElements/Titles";
import { Span } from "../components/TypographyElements/Span";
import { Info } from "../components/TypographyElements/Paragraphs";

export default function LoginPage() {
	const [loading, setLoading] = useState(false);

	return (
		<>
			<Section>
				<Container height="350px" justifyContent="space-between" alignItems="flex-start">
					<img src={figure} alt="An illustration of a woman talking" height="250px" />
					<Container alignSelf="center" justifySelf="left" flexDirection="column" width="100%">
						<Title>
							Lost in <Span>Translation</Span>
						</Title>
						<Subtitle> Get started &#8595;</Subtitle>
					</Container>
				</Container>
			</Section>
			<Section
				bgColor="var(--color-dark--1)"
				height="max(70vh)"
				alignItems="center"
				translateInner={"translateY(-120px)"}
			>
				<Container
					bgColor="var(--color-light--2)"
					height="220px"
					width="min(calc(1000px - 1rem), (100vw - 1rem))"
					alignItems="center"
					justifyContent="center"
					padding="2rem"
					borderRadius="10px"
				>
					{!loading ? (
						<LoginForm loading={loading} setLoading={setLoading} />
					) : (
						<Info>Logging you in. Please wait...</Info>
					)}
				</Container>
			</Section>
		</>
	);
}
