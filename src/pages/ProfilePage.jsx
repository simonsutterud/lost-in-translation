import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux/es/exports";

import Section from "../hoc/Section";
import Container from "../hoc/Container";
import Button from "../components/Buttons/Button";
import LastTranslationList from "../components/Translation/LastTranslationsList";
import LastTranslationListItem from "../components/Translation/LastTranslationsListItem";
import ClearButton from "../components/Buttons/ClearButton";

import { clearLocalStorage } from "../utils/localStorage";
import { loggedOut, updatedTranslations } from "../features/user/userSlice";
import { PageTitle } from "../components/TypographyElements/Titles";
import { deleteTranslations } from "../api/translations";
import { fetchUserByUsername } from "../api/user";

export default function ProfilePage() {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const { username, id: userId, translations } = useSelector((state) => state.user);

	useEffect(() => {
		fetchUserByUsername(username);
		dispatch(updatedTranslations(translations));
	}, [dispatch, translations, username]);

	const handleGoBack = () => navigate("/");

	const handleLogout = () => {
		clearLocalStorage("user");
		dispatch(loggedOut());
		navigate("/login");
	};

	const handleClearTranslations = () => {
		const confirm = window.confirm("Are you sure? This action can not be undone.");

		if (confirm) {
			deleteTranslations(userId);
			dispatch(updatedTranslations());
		}
	};

	return (
		<>
			<Section
				bgColor="var(--color-dark--1)"
				height="100vh"
				justifyContent="center"
				alignItems="center"
			>
				<PageTitle>Your last translations:</PageTitle>
				<Container
					margin="1rem 0 2rem 0"
					height="400px"
					width="min(calc(1000px - 2rem), (100vw - 1rem))"
					justifyContent="flex-start"
					alignItems="flex-start"
					bgColor="var(--color-light--2)"
					borderRadius="10px"
					overflow="auto"
				>
					<Container
						position="absolute"
						width="min(calc(1000px - 2rem), (100vw - 1rem))"
						justifyContent="flex-end"
						transform="translate(0, -3rem)"
					>
						<ClearButton onClick={handleClearTranslations}>CLEAR ALL</ClearButton>
					</Container>
					<LastTranslationList>
						{translations &&
							translations
								.slice(0, 10)
								.map((translation, i) => (
									<LastTranslationListItem key={i}>{translation}</LastTranslationListItem>
								))}
					</LastTranslationList>
				</Container>
				<Container width="min(calc(1000px - 2rem), (100vw - 1rem))" justifyContent="space-between">
					<Button primary onClick={handleGoBack}>
						BACK TO TRANSLATION PAGE
					</Button>
					<Button secondary onClick={handleLogout}>
						LOG OUT
					</Button>
				</Container>
			</Section>
		</>
	);
}
