import styled from "styled-components";

const Cont = styled.div`
	display: flex;
	box-sizing: border-box;
	align-content: flex-start;
	flex-wrap: ${(props) => props.flexWrap};
	overflow: ${(props) => props.overflow};
	flex-direction: ${(props) => props.flexDirection};
	justify-content: ${(props) => props.justifyContent};
	align-items: ${(props) => props.alignItems};
	align-self: ${(props) => props.alignSelf};
	justify-self: ${(props) => props.justifySelf};
	height: ${(props) => props.height};
	width: ${(props) => props.width};
	background-color: ${(props) => props.bgColor};
	color: ${(props) => props.color};
	font-weight: ${(props) => props.fontWeight};
	padding: ${(props) => props.padding};
	margin: ${(props) => props.margin};
	border-radius: ${(props) => props.borderRadius};
	position: ${(props) => props.position};
	transform: ${(props) => props.transform};
	z-index: ${(props) => props.zIndex};
`;

export default function Container(props) {
	return (
		<Cont
			position={props.position}
			transform={props.transform}
			flexDirection={props.flexDirection}
			justifyContent={props.justifyContent}
			alignItems={props.alignItems}
			alignSelf={props.alignSelf}
			justifySelf={props.justifySelf}
			height={props.height}
			width={props.width}
			bgColor={props.bgColor}
			color={props.color}
			padding={props.padding}
			margin={props.margin}
			borderRadius={props.borderRadius}
			fontWeight={props.fontWeight}
			overflow={props.overflow}
			flexWrap={props.flexWrap}
			zIndex={props.zIndex}
		>
			{props.children}
		</Cont>
	);
}
