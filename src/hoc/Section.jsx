import styled from "styled-components";

const Wrapper = styled.div`
	position: relative;
	width: 100vw;
	height: ${(props) => props.height};
	display: flex;
	justify-content: flex-start;
	align-items: center;
	background: ${(props) => props.bgColor};
	flex-direction: column;
`;

const Container = styled.section`
	position: relative;
	width: min(1000px, 100vw);
	padding: 2rem 1rem;
	box-sizing: border-box;
	display: flex;
	flex-direction: column;
	align-items: ${(props) => props.alignItems};
	transform: ${(props) => props.translateInner};
`;

export default function Section({ bgColor, children, height, alignItems, translateInner }) {
	return (
		<Wrapper bgColor={bgColor} height={height}>
			<Container alignItems={alignItems} translateInner={translateInner}>
				{children}
			</Container>
		</Wrapper>
	);
}
