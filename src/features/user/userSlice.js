import { createSlice } from "@reduxjs/toolkit";

const initialState = { id: null, username: null, translations: [] };

const userSlice = createSlice({
	name: "user",
	initialState,
	reducers: {
		loggedIn(state, action) {
			state.id = action.payload.id;
			state.username = action.payload.username;
			state.translations = action.payload.translations;
		},
		loggedOut(state) {
			state.id = null;
			state.username = null;
			state.translations = [];
		},
		updatedTranslations(state, action) {
			state.translations = action.payload;
		},
	},
});

export const { loggedIn, loggedOut, updatedTranslations } = userSlice.actions;

export default userSlice.reducer;
