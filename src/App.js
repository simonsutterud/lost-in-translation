import { Routes, Route, Navigate } from "react-router-dom";

import Navbar from "./components/Navbar/Navbar";
import LoginPage from "./pages/LoginPage";
import TranslationPage from "./pages/TranslationPage";
import ProfilePage from "./pages/ProfilePage";
import { getLocalStorage } from "./utils/localStorage";
import { useDispatch, useSelector } from "react-redux/es/exports";
import { loggedIn } from "./features/user/userSlice";
import { useEffect } from "react";

function App() {
	const dispatch = useDispatch();

	useEffect(() => {
		const user = getLocalStorage("user");
		if (user) {
			dispatch(loggedIn(user));
		}
	}, [dispatch]);

	const user = useSelector((state) => state.user);

	return (
		<>
			<Navbar />
			<Routes>
				{!user?.id ? (
					<>
						<Route path="/login" element={<LoginPage />} />
						<Route path="*" element={<Navigate to="/login" replace />} />
					</>
				) : (
					<>
						<Route path="/" element={<TranslationPage />} />
						<Route path="/profile" element={<ProfilePage />} />
						<Route path="*" element={<Navigate to="/" replace />} />
					</>
				)}
			</Routes>
		</>
	);
}

export default App;
