/**
 *
 * @param {string} key - The key you want to set in localStorage
 * @param {*} value - The value you want to attach to the specified key
 */
export function setLocalStorage(key, value) {
	localStorage.setItem(key, JSON.stringify(value));
}

/**
 *
 * @param {string} key - The key of the value you want to get from localStorage
 * @returns value of the key if it exists, else null
 */
export function getLocalStorage(key) {
	const data = localStorage.getItem(key);
	if (data) return JSON.parse(data);
	return null;
}

/**
 *
 * @param {string} key - The key of the value you want to remove from localStorage
 */
export function clearLocalStorage(key) {
	localStorage.clear(key);
}
