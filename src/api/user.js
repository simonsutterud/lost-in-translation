/**
 *
 * @returns Returns all existing users if success, error if not
 */
export async function fetchUsers() {
	try {
		const res = await fetch(process.env.REACT_APP_API_URL);
		if (!res.ok) throw new Error("Something went wrong.");

		const users = await res.json();
		return {
			users,
			error: null,
		};
	} catch (error) {
		return {
			users: [],
			error: error.message,
		};
	}
}

/**
 *
 * @param {string} username - The username of the user you want to fetch
 * @returns Returns the updated user if success, error message if not
 */
export async function fetchUserByUsername(username) {
	try {
		const res = await fetch(process.env.REACT_APP_API_URL + "?username=" + username);
		if (!res.ok) throw new Error("Something went wrong.");

		const user = await res.json();

		if (user.length === 0) throw new Error("No user exists");
		return {
			user,
			error: null,
		};
	} catch (error) {
		return {
			user: null,
			error: error.message,
		};
	}
}

/**
 *
 * @param {number} userId - The user ID of the user you want to fetch
 * @returns Returns the updated user if success, error message if not
 */
export async function fetchUserById(userId) {
	try {
		const res = await fetch(process.env.REACT_APP_API_URL + userId);
		if (!res.ok) throw new Error("Something went wrong.");

		const user = await res.json();

		if (user.length === 0) throw new Error("No such user exists");
		return {
			user,
			error: null,
		};
	} catch (error) {
		return {
			user: null,
			error: error.message,
		};
	}
}

/**
 *
 * @param {string} username - The username of the user you want to create
 * @returns Returns the created user if success, error message if not
 */
export async function createUser(username) {
	try {
		const res = await fetch(process.env.REACT_APP_API_URL, {
			method: "POST",
			headers: {
				"X-API-KEY": process.env.REACT_APP_API_KEY,
				"content-type": "application/json",
			},
			body: JSON.stringify({
				username,
				translations: [],
			}),
		});

		if (!res.ok) throw new Error("Could not create new user!");

		const user = await res.json();
		return {
			user,
			error: null,
		};
	} catch (error) {
		return {
			user: null,
			error: error.message,
		};
	}
}
