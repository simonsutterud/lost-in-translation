import { fetchUserById } from "./user";

/**
 *
 * @param {number} userId - The user ID of the user which the translation belongs to.
 * @param {string} translation - The translation you want to store.
 * @returns Returns the updated user if success, error message if not
 */
export async function saveTranslation(userId, translation) {
	try {
		const data = await fetchUserById(userId);

		const existingTranslations = data.user.translations;

		const res = await fetch(process.env.REACT_APP_API_URL + userId, {
			method: "PATCH",
			headers: {
				"X-API-KEY": process.env.REACT_APP_API_KEY,
				"content-type": "application/json",
			},
			body: JSON.stringify({
				translations: [translation, ...existingTranslations],
			}),
		});

		if (!res.ok) throw new Error("Could not add new translation!");

		const user = await res.json();

		return {
			user,
			error: null,
		};
	} catch (error) {
		return {
			user: null,
			error: error.message,
		};
	}
}

/**
 *
 * @param {number} userId - The user ID of the user you want to delete translations for.
 * @returns Returns the updated user if success, error message if not
 */
export async function deleteTranslations(userId) {
	try {
		const res = await fetch(process.env.REACT_APP_API_URL + userId, {
			method: "PATCH",
			headers: {
				"X-API-KEY": process.env.REACT_APP_API_KEY,
				"content-type": "application/json",
			},
			body: JSON.stringify({
				translations: [],
			}),
		});

		if (!res.ok) throw new Error("Could not delete translations!");

		const user = await res.json();

		return {
			user,
			error: null,
		};
	} catch (error) {
		return {
			user: null,
			error: error.message,
		};
	}
}
